import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;


public class HelloWorldExample extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		
		//ResourceBundle rb = ResourceBundle.getBundle("LocalStrings", request.getLocale());
		
		try {

			out.println("<!DOCTYPE html>");
			out.println("<html> <head>");
			out.println("</head>");
			out.println("<body> <h1>" + "hello WORLD" + "</h1> </body> </html>");
		} finally {
			out.close();
		}
		
	}
}

